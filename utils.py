import os
import subprocess

import h5py
import numpy as np
import pandas as pd

from c3d_feature_helper import Feature

def call_vlad_batch_encoding(kmeans_model, feat_dir, output_dir,
                             mdriver, vlfeat):
    """ Calls Matlab application that encodes a batch of C3D features with VLAD.

    Parameters
    ----------
    kmeans_model : str
        path to hdf5 file containing the K-Means model.
    feat_dir : str
        directory containing batch of C3D features (one file per segment)
    output_dir : str
        directory where encoded VLAD features will be stored.
    mdriver : str
        path to where the Matlab applications are located.
    vlfeat : str
        path to vlfeat toolbox.
    """
    kmeans_model = os.path.abspath(kmeans_model)
    feat_dir = os.path.abspath(feat_dir)
    output_dir = os.path.abspath(output_dir)
    cmd = ("matlab -r \"try addpath('{}'); vl_setup; addpath('{}'); "
           "batch_vlad_segment_encoding('{}', '{}', '{}'); catch; end; quit\""
           ">> /tmp/matlab_dump.tmp".format(vlfeat, mdriver, kmeans_model,
                                            feat_dir, output_dir))
    proc = subprocess.Popen(cmd, shell=True)
    proc.communicate()

def dump_segments(df, feature_file, dataset_path, output_filename,
                  fname_format='{0:04d}'):
    """ Dump C3D segments from a C3D complete dataset.

    Parameters
    ----------
    df : DataFrame
        contains the info of the segments to dump.
    feature_file : str
        path to hdf5 file containing the raw C3D features.
    dataset_path : str
        path where C3D segment features will be dumped.
    output_filename : str
        path where a csv file will be saved. This file will contain info about
        the dumped segments.
    """
    # Read annotations from DataFrame.
    df = df.reset_index()
    fname_lst = []
    feat_obj = Feature(feature_file, pool_type=None)
    feat_obj.open_instance()
    true_idx = []

    # Dumps C3D features for each segment.
    for idx, v in df.iterrows():
        this_filename = os.path.join(dataset_path,
                                     '{}.hdf5'.format(fname_format.format(idx)))
        try:
            feat = feat_obj.read_feat(v['video-name'], v['f-init'],
                                      v['f-end']-v['f-init']+1)
            f = h5py.File(this_filename, 'w')
            f.create_dataset('data', data=feat)
            f.close()
            fname_lst.append(fname_format.format(idx))
            true_idx.append(idx)
        except:
            print ('Warning: Unable to dump segment: {}, '
                   'f-init={}, f-end={}'.format(v['video-name'],
                                                v['f-init'], v['f-end']))

    # Keeps a record of the segments that were dumped.
    dset = pd.DataFrame({'video-name': df['video-name'].iloc[true_idx],
                         'filename': fname_lst,
                         'f-init': df['f-init'].iloc[true_idx],
                         'f-end': df['f-end'].iloc[true_idx],
                         'score': df['score'].iloc[true_idx]})
    dset.to_csv(output_filename, sep=' ', index=False)

def load_feature_matrix(df, vlad_path):
    """Returns a matrix X where each row is segment encoded with VLAD."""
    # Define sizes.
    f_0 = read_feature(os.path.join(vlad_path,
                                    '{0:04d}.hdf5'.format(df.iloc[0]['filename'])))
    n_samples = df.shape[0]
    n_dims = f_0.shape[0]
    # Getting the feature matrix.
    X = np.zeros((n_samples, n_dims))
    i = 0
    for k, v in df.iterrows():
        filename = os.path.join(vlad_path, '{0:04d}.hdf5'.format(v['filename']))
        if not os.path.exists(filename):
            raise RuntimeError('Feature does not exists: {}'.format(filename))
        X[i, :] = read_feature(filename)
        i += 1
    return X

def nms_detections(dets, score, overlap=0.3):
    """
    Non-maximum suppression: Greedily select high-scoring detections and
    skip detections that are significantly covered by a previously
    selected detection.
    This version is translated from Matlab code by Tomasz Malisiewicz,
    who sped up Pedro Felzenszwalb's code.
    Parameters
    ----------
    dets : ndarray.
        Each row is ['f-init', 'f-end']
    score : 1darray.
        Detection score.
    overlap : float.
        Minimum overlap ratio (0.3 default).
    Outputs
    -------
    dets : ndarray.
        Remaining after suppression.
    """
    t1 = dets[:, 0]
    t2 = dets[:, 1]
    ind = np.argsort(score)

    area = (t2 - t1 + 1).astype(float)

    pick = []
    while len(ind) > 0:
        i = ind[-1]
        pick.append(i)
        ind = ind[:-1]

        tt1 = np.maximum(t1[i], t1[ind])
        tt2 = np.minimum(t2[i], t2[ind])

        wh = np.maximum(0., tt2 - tt1 + 1.0)
        o = wh / (area[i] + area[ind] - wh)

        ind = ind[np.nonzero(o <= overlap)[0]]

    return pick

def read_feature(f):
    """Reads VLAD features and load it into memory."""
    with h5py.File(f, 'r')  as fobj:
        feat = fobj['data'].value
    return feat
