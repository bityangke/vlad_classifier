import argparse
import os
import pickle

import h5py
import numpy as np
import pandas as pd

from sklearn import svm

def read_feature(f):
    with h5py.File(f, 'r')  as fobj:
        feat = fobj['data'].value
    return feat

def get_dataset(df, feature_path):
    # Define sizes.
    f_0 = read_feature(os.path.join(feature_path,
                                    '{0:04d}.hdf5'.format(df.iloc[0]['filename'])))
    n_samples = df.shape[0]
    n_dims = f_0.shape[0]

    # Getting the label matrix.
    mapped_labels = df['label-idx'].unique()
    Y = np.zeros((n_samples, mapped_labels.shape[0]))
    for i, label in enumerate(mapped_labels):
        Y[:, i] = (df['label-idx'] == label).as_matrix().astype(np.int)

    # Getting the feature matrix.
    X = np.zeros((n_samples, n_dims))
    i = 0
    for k, v in df.iterrows():
        filename = os.path.join(feature_path, '{0:04d}.hdf5'.format(v['filename']))
        if not os.path.exists(filename):
            raise RuntimeError('Feature does not exists: {}'.format(filename))
        X[i, :] = read_feature(filename)
        i += 1
    return X, Y, mapped_labels

def main(dataset_info, feature_path, model_filename,
         feature_matrix_filename=None, C=100, verbose=True):
    # Loading dataset.
    df = pd.read_csv(dataset_info, sep=' ')
    X, Y_matrix, mapped_labels = get_dataset(df, feature_path)
    mapped_labels = mapped_labels.tolist()
    if feature_matrix_filename:
        with h5py.File(feature_matrix_filename, 'w') as fobj:
            fobj.create_dataset('X', data=X)
            fobj.create_dataset('Y_matrix', data=Y_matrix)
            fobj.create_dataset('mapped_labels', data=mapped_labels)

    # Learning models.
    model_dict = {'models': None, 'config': {'C': C, 'classes': mapped_labels}}
    model_dict['models'] = {label:None for label in mapped_labels}
    for label, m in model_dict['models'].iteritems():
        cidx = mapped_labels.index(label)
        if cidx < 0: continue; # Skipping "background" classes.
        Y = Y_matrix[:, cidx]
        model = svm.LinearSVC(C=C, class_weight='auto', verbose=True)
        model.fit(X, Y)
        model_dict['models'][label] = model
        if verbose:
            print 'Trained model for class: {}'.format(label)

    # Saving models.
    with open(model_filename, 'wb') as fobj:
        pickle.dump(model_dict, fobj)
    if verbose:
        print 'Model sucessfully saved at: {}'.format(model_filename)

if __name__ == '__main__':
    description = 'Train linear SVM model on vlad features.'
    p = argparse.ArgumentParser(description=description)
    p.add_argument('dataset_info',
                   help='csv file containing the dataset annotations.')
    p.add_argument('feature_path',
                   help='Path where vlad features are located.')
    p.add_argument('model_filename',
                   help='Pickle file where model will be saved.')
    p.add_argument('feature_matrix_filename',
                   help='Where to store the feature vector matrix.')
    p.add_argument('--C', default=100, type=float,
                   help='svm hyperparameter.')
    p.add_argument('--verbose', default=True, type=bool)
    main(**vars(p.parse_args()))
