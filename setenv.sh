# Comment the following lines if you do not have the modules available
# but you must check that matlab and anaconda are in your path.
module purge
module load anaconda
module load matlab

# Download VLFeat binaries if do not exists.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VLFEAT_DIR=$DIR/matlab/vlfeat
VLFEAT_URL='http://www.vlfeat.org/download/vlfeat-0.9.20-bin.tar.gz'
if [ ! -d "$VLFEAT_DIR" ]; then
  wget -qO- $VLFEAT_URL | tar xvz -C $DIR/matlab
  mv $DIR/matlab/vlfeat* $DIR/matlab/vlfeat
fi
